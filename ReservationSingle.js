import React, {useState, useEffect} from 'react';
import TableProduct from './partials/TableProduct';
import {useParams} from 'react-router-dom';
import ReservationHeader from './partials/ReservationHeader';

const ReservationSingle = (props) => {

    const [reservation, setReservation] = useState({
        _id: "",
        customerId:{
            fullname:""
        }
    
    });
    const {id} = useParams();
    useEffect(()=>{
        fetch(`https://tarabundokbackend.herokuapp.com/transactions/${id}`,{
            headers: {
                "Authorization": `Bearer ${localStorage["appState"]}`
            }
        })
        
        .then(res =>res.json())
        .then(data => {
            if(data){
                setIsLoading(false)
            }
            setReservation(data)})
    },[])

    const[isLoading, setIsLoading] = useState(true)
  return (
    <div className="container">
    	<div className="row">
    		<div className="col-12">
    			
                {
                            !isLoading ?
                            <ReservationHeader reservation={reservation}/> :
                            "Loading..."
                        }
    		</div>
    		<div className="col-12">

                {
                    reservation.orders? 
                    <TableProduct orders={reservation.orders}/>:
                    "Loading..."
                }
    			
    		</div>
    	</div>
    </div>
  )
}

export default ReservationSingle;