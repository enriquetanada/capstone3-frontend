import React, {useState, useEffect} from 'react';
import Home from './components/Home';
import Login from './components/Login';
import RegisterForm from './components/RegisterForm';
import MainNav from './components/MainNav';
import Destination from './components/Destination';
import CreateDestination from './components/CreateDestination';
import Cart from './components/Cart';
import DestinationSingle from './components/DestinationSingle';
import Logout from './components/Logout';
import MountainEdit from './components/MountainEdit';
import Reservation from './components//Reservation';
import ReservationSingle from './components/ReservationSingle';
import './css/Home.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

function App() {

  const [authUser, setAuthUser] = useState({
    isAuth:false,
    _id:"",
    fullname:"",
    email:""
  })
  useEffect(()=>{
    let appState = localStorage['appState']

    if(appState){
      fetch("https://tarabundokbackend.herokuapp.com/users/profile",{
        headers:{
          "Authorization" : `Bearer ${appState}`
        }
      })
      .then(res =>res.json())
      .then(data=>{
        if(data._id){
          setAuthUser({
            isAuth:true,
            _id: data._id,
            fullname:data.fullname,
            email:data.email,
            isAdmin: data.isAdmin
          })
        }
             })
    }
  },{})

  
  
  return (
    <Router>
      <MainNav authUser={authUser} />
    
      <Switch>
        <Route path = "/login">
          <Login authUser={authUser} setAuthUser={setAuthUser} />
        </Route>

        <Route path = "/register">
          <RegisterForm />
        </Route>

        <Route path = "/destination">
          <Destination authUser={authUser} setAuthUser={setAuthUser}/>
        </Route>

        <Route exact path = "/mountains/:id/edit">
          <MountainEdit />
        </Route>

        <Route path = "/createdestination">
          <CreateDestination />
        </Route>

        <Route exact path = "/destinations/:id">
          <DestinationSingle />
        </Route>

        <Route exact path= "/reservations/:id">
          <ReservationSingle authUser={authUser} />
        </Route>

        <Route exact path ="/reservations" >
          <Reservation authUser={authUser}/>
        </Route>

        <Route exact path ="/logout">
            <Logout setAuthUser={setAuthUser}/>
        </Route>

        <Route path = "/books/:id">
          <Cart />
        </Route>
        

        <Route path = "/">
          <Home authUser={authUser}/>
        </Route>

      </Switch>

    </Router>
  );
}

export default App;
