import React, {useState} from 'react';
import AlertMessage from './partials/AlertMessage'
import {Redirect} from 'react-router-dom';
import swal from 'sweetalert';

const Register = (props) => {

	const [user, setUser] = useState({
		fullname :"",
		email: "",
		password: "",
		confirmPassword: ""
	})

	const handleChange = e =>{
       setUser({
            ...user,
            [e.target.name] : e.target.value
        })
    }

    const [error, setError] = useState({
    	hasError: false,
    	color:"",
    	message:""
    })

    const[isSuccess, setIsSuccess] =useState(false)

    if(isSuccess){
    	swal({
                    title: "Registration Successful!",
                    icon:"success",
                    Button: "Ok",
                    timer: 3000
                })
    	return <Redirect to="/login" />
    }

	const handleSubmit = e =>{
		e.preventDefault()

		fetch("https://tarabundokbackend.herokuapp.com/users/register",{
			method: "post",
            body: JSON.stringify(user),
            headers: {
                'Content-Type' : 'application/json'
			}
		})
		.then(res =>{
			
			if(res.status === 400){
				swal({
					title: "Register Failed!",
					text: "Please check your credentials",
					icon:"error",
					Button: "Ok",
					timer: 3000
				})
			}
			else{
                setIsSuccess(true)
            }
			return res.json()
		})
		.then(data =>{
			console.log(data)
		})
	}

  return (
    <div className = "container">
	    <div className = "row my-5" >
		    <div className = "col-12 col-sm-10 col-md-8 col-lg-6 mx-auto">
		    	<div className="card">
			    	<div className="card-header">
				    	<h2 className="text-center"> Registration Page </h2>
					</div>
					<div className="card-body">
					    <form onSubmit={handleSubmit}>
					    {
					    	error.hasError ?
					    	<AlertMessage 
					    		color={error.color}
					    		message={error.message}
					    	/> 
					    	:
					    	<></>

					    }
							<div className="form-group">
								<div className="input-group mb-3">
									<div className="input-group-prepend">
									   	<span className="input-group-text">
									    	<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  												<path fillRule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
											</svg>
									    </span>
									</div>
									<input 
									 	type="text" 
									 	className="form-control"
									 	name="fullname" 
									 	placeholder="Full name"
									 	onChange={handleChange} 
									 />
								</div>
							</div>

							<div className="form-group">
								<div className="input-group mb-3">
									<div className="input-group-prepend">
									   	<span className="input-group-text">
									    	<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-envelope-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
												<path fillRule="evenodd" d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
											</svg>
									    </span>
									</div>
									<input 
									 	type="email" 
									 	className="form-control" 
									 	name="email" 
									 	placeholder="Email" 
									 	onChange={handleChange} 

									 />
								</div>
							</div>
						  	<div className="form-group">
								<div className="input-group mb-3">
									<div className="input-group-prepend">
									   	<span className="input-group-text">
									    	<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-lock-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
												<path d="M2.5 9a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-7a2 2 0 0 1-2-2V9z"/>
												<path fillRule="evenodd" d="M4.5 4a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z"/>
											</svg>
									    </span>
									</div>
									<input 	
									 	type="password" 
									 	className="form-control"
									 	name="password" 
									 	placeholder="Password"
									 	onChange={handleChange} 

									 />
								</div>
							</div>
							<div className="form-group">
								<div className="input-group mb-3">
									<div className="input-group-prepend">
									   	<span className="input-group-text">
									    	<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-lock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  												<path fillRule="evenodd" d="M11.5 8h-7a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1zm-7-1a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-7zm0-3a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z"/>
											</svg>
									    </span>
									</div>
									<input 
									 	type="password" 
									 	className="form-control" 
									 	name="confirmPassword" 
									 	placeholder="Confirm Password"
									 	onChange={handleChange} 

									 />
								</div>
							</div>
						  <button type="submit" className="btn btn-primary d-block mx-auto w-50">Register</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
  )
}

export default Register;