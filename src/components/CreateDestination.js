import React, {useState, useEffect} from 'react';
import InputGroup from './partials/InputGroup'
import {Redirect} from 'react-router-dom';
import swal from 'sweetalert';

const CreateDestination = (props) => {
    const [isLoading, setIsLoading]= useState(true);
    
    const [isRedirect, setIsRedirect]= useState({
        latestId: "",
        success: false
    })

    const [mountain, setMountain]= useState({
        name:"",
        rate:"",
        description:"",
        itinerary:"",
        information:"",
        image:{}
    })

    if(isRedirect.success){
        swal({
                    title: "Added Destination Successfully!",
                    icon:"success",
                    Button: "Ok",
                    timer: 3000
                })

        return <Redirect to="/destination" />
    }
    const handleChange = e=>{
        setMountain({
            ...mountain,
            [e.target.name] : e.target.value

        })
       
    }


    const handleChangeFile = e =>{
        setMountain({
            ...mountain,
            image: e.target.files[0]
        })
    }

    const handleSubmit = e =>{
        e.preventDefault();
        let formData= new FormData();

        formData.append('name', mountain.name)
        formData.append('rate', mountain.rate)
        formData.append('description', mountain.description)
        formData.append('itinerary', mountain.itinerary)
        formData.append('information', mountain.information)
        formData.append('image', mountain.image)

        fetch('https://tarabundokbackend.herokuapp.com/mountains',{
            method: "post",
            body: formData,
            headers:{
                "Authorization": `Bearer ${localStorage['appState']}`
            }
        })
        .then(res=>{
            if(res.status === 400){
                swal({
                    title: "Add Destination Failed!",
                    text: "Please complete all fields",
                    icon:"error",
                    Button: "Ok",
                    timer: 3000
                })
            }

            return res.json()})
        .then(data=>{
            if(data._id){
                setIsRedirect({
                    latestId:data._id,
                    success:true

                })
            }
        })
    }
  return (
    <div className="container my-2">
    	<div className="row">
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
    			<form onSubmit={handleSubmit}>
    				<InputGroup
    					name="name"
    					displayName="Mountain Name:"
    					type="text"
                        handleChange={handleChange}
    				/>
    				<InputGroup
    					name="rate"
    					displayName="Rate:"
    					type="number"
                        handleChange={handleChange}

    				/>
    				<label htmlFor="description">Description:</label>
    				<textarea 
    						name="description" 
    						id="description" 
    						cols="30" 
    						rows="5"
    						className="form-control"
                            onChange={handleChange}></textarea>

    				<label htmlFor="information">Information:</label>
    				<textarea 
    						name="information" 
    						id="information" 
    						cols="30" 
    						rows="5"
    						className="form-control"
                        onChange={handleChange}
                            ></textarea>

    				<label htmlFor="itinerary">Itinerary:</label>
    				<textarea 
    						name="itinerary" 
    						id="itinerary" 
    						cols="30" 
    						rows="5"
    						className="form-control"
                        onChange={handleChange}
                            ></textarea>

    				<InputGroup 
    					name ="image"
    					displayName="Image:"
    					type="file"
                        handleChange={handleChangeFile}



    				/>

    				<button className="btn btn-primary my-3">Add Mountain</button>
    			</form>	
    		</div>
    	</div>

    </div>
  )
}

export default CreateDestination;