import React, {useState, useEffect} from 'react';
import {Redirect} from 'react-router-dom';

const Logout = (props) => {

	const [isRedirect, setIsRedirect] = useState(false)

	useEffect(()=>{
		localStorage.removeItem('appState')
		props.setAuthUser ({
			isAuth:false,
			_id:"",
			fullname:"",
			email:""
		})
		setIsRedirect(true)
	},{})

	if(isRedirect){
		return <Redirect to="/" />
	}
  return (
    <div>Logging out please wait a moment</div>
  )
}

export default Logout;