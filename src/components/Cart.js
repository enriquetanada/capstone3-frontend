import React, {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import InputGroup from './partials/InputGroup';
import moment from'moment';
import {Redirect} from 'react-router-dom';



const Cart = (props) => {

	const {id} = useParams();

    const [mountain, setMountain] = useState({
        name:"",
        rate:"",
        description:"",
        itinerary:"",
        information:"",
        image:""
    })

    const [isLoading, setIsLoading] = useState(true)
    const[isRedirect, setIsRedirect] = useState({
      latestId: "",
        success: false
    })
    const[person, setPerson] = useState({})
    const [cart, setCart] = useState({
    dateReserved: "",
    orders:[]
    });
    const [dateReserved, setDateReserved] = useState(new Date())
console.log(cart)
    const [state, setState] = useState({
      startDate: new Date()
      
    });

    useEffect(()=>{
        fetch(`https://tarabundokbackend.herokuapp.com/mountains/${id}`)
        .then(res =>res.json())
        .then(data=>{
            setMountain(data)
            setIsLoading(false)
        })
    },[])

    if(isRedirect.success){
      return <Redirect to={`/reservations/${isRedirect.latestId}`} />
    }
    

    
  	const handleChangeDate = date => {
    	setDateReserved(date);
  	};

  	const handleChange = e=>{
  		setPerson({
  		[e.target.name] : e.target.value
  	})

      setCart({
        dateReserved: dateReserved.toDateString(),
        orders: [
        {
          mountainId: mountain._id,
          
          [e.target.name]: e.target.value
        }
          

        ]
      })
	}
console.log(dateReserved)
  // console.log(cart)
  	const handleSubmit =e =>{
  	e.preventDefault()
    
    // console.log(mountain._id)
    // addToCart(mountain._id,person.person)
    // console.log(state.startDate)
    fetch('https://tarabundokbackend.herokuapp.com/reservations',{
      method: "post",
      body: JSON.stringify(cart),
            headers: {
               "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage['appState']}`
      }
    })

    .then(res =>res.json())
    .then(data=>{
      if(data._id){
        console.log(data)
        setIsRedirect({
          latestId: data._id,
          success:true
        })
      }
    })

	}
	const dt = new Date();
        const minDate = dt.setDate(dt.getDate() + 1);
        
        
  return (
  	
    <div className="container ">
    	<div className="row mt-5">
    		<div className="col-12 col-md-6 col-lg-6">
	    		<div className="card cart-home">
	    			<img src={`https://tarabundokbackend.herokuapp.com/${mountain.image}`} className="card-img-top cart-image" />
	    		</div>
    		</div>
    		
    		<div className="col-12 col-md-6 col-lg-6 ">
    			<h2 className="text-center">Booking Form</h2>
    		<form onSubmit = {handleSubmit}>
          <h5>Rate Per Head:&#8369;{mountain.rate}</h5>
        <InputGroup 
        type="number"
        name="cost"
        displayName="Cost"
        handleChange={handleChange}
        value={person && person.person <=12 && person.person> 1?  mountain.rate*person.person: "0"}
        disabled
      />
      <h5> Date </h5>
          <DatePicker
            selected={dateReserved}
            onChange={handleChangeDate}
            minDate={minDate}
            dateFormat="yyyy/MM/dd"
          />
    		<InputGroup 
				type="number"
				name="person"
				displayName="Person"
				min="1"
				max="12"
				handleChange={handleChange}
			/>
      

      <h4>Total: &#8369;{person && person.person >=6 && person.person <=12?  (mountain.rate*person.person)-((mountain.rate*person.person)*.2): (person && person.person <6 && person.person >=1 ? mountain.rate*person.person : "0" ) }</h4>
      <button> submit</button>
			</form>
				
    			
    		</div>

    		<div className="col-12 col-md-6 col-lg-6 mt-2">
    		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				  <li class="nav-item col-4 col-md-3 col-lg-4">
				    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Description</a>
				  </li>
				  <li class="nav-item col-4 col-md-3 col-lg-4">
				    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Itinerary</a>
				  </li>
				  <li class="nav-item col-4 col-md-3 col-lg-4">
				    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Information</a>
				  </li>
				</ul>
				<div class="tab-content" id="pills-tabContent">
				  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">{mountain.description}</div>
				  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">{mountain.itinerary}</div>
				  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">{mountain.information}</div>
				</div>	
			</div>
    	</div>
 
    </div>

  )
}

export default Cart;