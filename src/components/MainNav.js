import React from 'react';
import {Link, NavLink} from 'react-router-dom'

const MainNav = ({authUser}) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success ">
	  <Link className="navbar-brand" to="/"><i className="fas fa-hiking"></i>Tara Bundok!</Link>
	  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span className="navbar-toggler-icon"></span>
	  </button>
	  <div className="collapse navbar-collapse" id="navbarNav">
	    <ul className="navbar-nav mr-auto">
	    {
	    	authUser.isAdmin ? 
	    	<li className="nav-item">
	        	<NavLink className="nav-link" exact to="/createdestination">Create Destination</NavLink>
	      	</li>
	      	:
	      	""
	    }
	      	
	    </ul>
	    <ul className="navbar-nav ml-auto">
	    	<li className="nav-item">
	        	<NavLink className="nav-link" to="/destination">Destinations</NavLink>
	      	</li>
	    	
	      	{
	      		!authUser.isAuth ?
	      		<>
		      		<li className="nav-item">
		        	<NavLink className="nav-link" to="/register">Register</NavLink>
			      	</li>
			      	<li className="nav-item">
			        	<NavLink className="nav-link" to="/login">Login</NavLink>
			      	</li>
			    </>
			      	:
			      	<>
			      	<li className="nav-item">
	        		<NavLink className="nav-link" to="/reservations">Bookings</NavLink>
	      			</li>
			      	<li className="nav-item">
			        	<NavLink className="nav-link" to="/logout">Logout</NavLink>
			      	</li>
			      	</>

	      	}
	      	
	    </ul>
	  </div>
	</nav>
  )
}

export default MainNav;
