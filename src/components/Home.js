import React,{useState, useEffect} from 'react';
import './../css/Home.css';	
import {Link} from 'react-router-dom';
import CardImage from './partials/CardImage';
import Benefit from './partials/Benefit'
import Parallax from './partials/Parallax'

const Home = ({authUser}) => {
const [mountains, setMountain] = useState([])
 const [deletedMountain, setDeletedMountain] = useState({})


	useEffect(()=>{
		fetch("https://tarabundokbackend.herokuapp.com/mountains")
		.then(res => {return res.json()})
		.then(mountains=>setMountain(mountains))
	},[])

	 useEffect(()=>{
    if(deletedMountain){
      setMountain(mountains.filter( mountain => {
        return mountain._id !== deletedMountain._id
      }))
    }
  },[deletedMountain]);

	let mountainList = mountains.map(mountain=>(
		<CardImage mountain={mountain} setDeletedMountain={setDeletedMountain} authUser={authUser}/>
	))
  return (
  <>
  	<div className="container-fluid px-0">
	    <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
			<ol className="carousel-indicators">
			    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
			    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div className="carousel-inner">
			    <div className="carousel-item active">
			      <img 
			      	className="d-block w-100 " 
			      	id="image-home"
			      	src={require("./../images/pulag1.jpg")} 
			      	alt="First slide"/>
			    </div>
			    <div className="carousel-item">
			      <img 
			      	className="d-block w-100 image-home"
			      	id="image-home" 
			      	src={require("./../images/pinatubo.jpg")} 
			      	alt="Second slide"/>
			    </div>
			    <div className="carousel-item">
			      <img 
			      	className="d-block w-100 image-home" 
			      	id="image-home"
			      	src={require("./../images/guiting.jpg")} 
			      	alt="Third slide"/>
			    </div>
			</div>
			<a 
				className="carousel-control-prev" 
				href="#carouselExampleIndicators" 
				role="button" 
				data-slide="prev"
			>
			    <span 
			    	className="carousel-control-prev-icon" 
			    	aria-hidden="true"
			    >
			    </span>
			    <span 
			    	className="sr-only"
			    >
					Previous
				</span>
			</a>
			<a 
				className="carousel-control-next" 
				href="#carouselExampleIndicators" 
				role="button" data-slide="next"
			>
			    <span 
			    	className="carousel-control-next-icon" 
			    	aria-hidden="true"
			    >
			    </span>
			    <span 
			    	className="sr-only"
			    >
					Next
				</span>
			</a>
		</div>
		<div className="main-text">
			<div className="col-md-12 text-center">
				<h1 className="explore">EXPLORE</h1>
				<h2 className="mountains">Mountains</h2>
				<h6 className="difference">Take the <strong>CHALLENGE</strong> and check our <strong>HASSLE FREE</strong> tour</h6>
			</div>
			<div className="text-center ">
				<Link className="btn btn-success home-button" to="/">View Mountains</Link>
			</div>
		</div>
	
	<div className="container">
		<h1 className="text-center" id="body-home">DESTINATIONS</h1>
  		<hr />
  		<h4 className="text-center">Explore our latest destinations</h4>
  		<div className="row">	
				{mountainList}

  		</div>
	</div>
	
	</div>
	

	<div className="container-fluid parallax-body my-0 ">
		<Parallax />
	</div>
	<div className="container-fluid benefit-body pt-5 ">
		<Benefit />
	</div>
	


	</>
  )
}

export default Home;	