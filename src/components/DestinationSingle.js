import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import CardImage from './partials/CardImage';

const DestinationSingle = (props) => {
	let {id} = useParams();

	const[mountain, setMountain] = useState({})
	const[isLoading, setIsLoading] = useState(true)

	useEffect(()=>{
		fetch(`https://tarabundokbackend.herokuapp.com/mountains/${id}`)
		.then(res =>res.json())
		.then(data=>{
			setMountain(data)
			setIsLoading(false)
		})
	},[])
  return (
    <div className="container">
    	<div className="row">
    		<div className="card">	
    		<img src="{`https://tarabundokbackend.herokuapp.com/${mountain.image}`}" className="card-img-top" />
    		
    		</div>
    	</div>
    </div>
  )
}

export default DestinationSingle;