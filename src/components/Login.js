import React, {useState} from 'react';
import {NavLink,Redirect} from 'react-router-dom';
import AlertMessage from './partials/AlertMessage';
import swal from 'sweetalert';

const Login = (props) => {
	

	const[credentials, setCredentials] = useState({
		email: "",
		password:""
	})

	const[alert, setAlert] = useState({
		hasAlert:false,
		color:"",
		message:""
	})

	const[isLogin, setIsLogin] = useState(false)
	const[isLoading, setIsLoading] = useState(false)

	if(isLogin){
		swal({
                    title: "Login Successful!",
                    icon:"success",
                    Button: "Ok",
                    timer: 3000
                })
		return <Redirect to="/" />
	}

	const handleChange = e =>{
		setCredentials({
			...credentials,
			[e.target.name] : e.target.value
		})
	}



	const handleSubmit = e=>{
		e.preventDefault()
		setIsLoading(true)
		fetch("https://tarabundokbackend.herokuapp.com/users/login",{
			method : "post",
			body: JSON.stringify(credentials),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then(res =>{
			if(res.status !== 200){
				swal({
					title: "Login Failed!",
					text: "Please check your credentials",
					icon:"error",
					Button: "Ok",
					timer: 1500
				})
			}
			return res.json()})
		.then(data=>{
			if(data.token){
				localStorage['appState'] = data.token
				props.setAuthUser({
					isAuth:true,
					fullname:data.fullname,
					email:data.email,
					isAdmin:data.isAdmin
				})
				setIsLogin(true)
			}else{
				setIsLoading(false)
			}
		})
	}
	if(props.authUser.isAuth){
		return <Redirect to="/" />
	}
  return (
    <div className = "container">
	    <div className = "row my-5" >
	    	<div className="col-12 col-sm-10 col-md-8 col-lg-6 mx-auto">
			    <div className = "card ">
			    	<div className="card-header">
			    		<h2 className="text-center">Login</h2>
			    	</div>
			    	<div className="card-body">
					    <form onSubmit={handleSubmit}>
					    {
					    	alert.hasAlert ?
					    	<AlertMessage 
					    		color={alert.color}
					    		message={alert.message}/>
					    	:
					    	<></>
					    }
							<div className="form-group">
							    <div className="input-group mb-3">
								  	<div className="input-group-prepend">
								    	<span className="input-group-text">
								    		<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-envelope-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
											 	<path fillRule="evenodd" d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
											</svg>
								    	</span>
								  	</div>
								 	<input 
								 		type="email" 
								 		className="form-control" 
								 		name="email" 
								 		placeholder="Email"
								 		onChange = {handleChange} 
								 	/>
								</div>
							</div>
							<div className="input-group mb-3">
								<div className="input-group-prepend">
								    <span className="input-group-text" id="basic-addon1">
								    	<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-lock-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
										<path d="M2.5 9a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-7a2 2 0 0 1-2-2V9z"/>
										<path fillRule="evenodd" d="M4.5 4a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z"/>
										</svg>
								    </span>
								</div>
								<input 
								  	type="password" 
								  	className="form-control"
								  	name="password" 
								  	placeholder="Password"
								 	onChange = {handleChange} 

								/>
							</div>
						  	<small className="text-center">Don't have an account? <NavLink to="/register">Register Here! </NavLink> </small>
						  	<button 
						  			type="submit" 
						  			className="btn btn-primary d-block mx-auto w-50 my-5"
						  			disabled={isLoading}
						  	>
						  		{
						  			isLoading ? "Login..." : "Login"
						  		}

						  	</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
  )
}

export default Login;