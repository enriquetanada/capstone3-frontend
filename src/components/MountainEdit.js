import React, {useState, useEffect} from 'react';
import InputGroup from './partials/InputGroup'
import {useParams} from 'react-router-dom';
import {Redirect} from 'react-router-dom';
import swal from 'sweetalert'

const MountainEdit = (props) => {

    const {id} = useParams();

    const [mountain, setMountain] = useState({
        name:"",
        rate:"",
        description:"",
        itinerary:"",
        information:"",
        image:""
    })
    let count=0;
    const [isLoading, setIsLoading] = useState(true)
    const[isRedirect, setIsRedirect] = useState(false)


    useEffect(()=>{
        fetch(`https://tarabundokbackend.herokuapp.com/mountains/${id}`)
        .then(res =>res.json())
        .then(data=>{
            setMountain(data)
            setIsLoading(false)
        })
    },[])

    const handleChange =e =>{
        setMountain ({
            ...mountain,
            [e.target.name] : e.target.value
        })
    }

    const handleChangeFile = e =>{
        setMountain({
            ...mountain,
            image:e.target.files[0]
        })
    }
    const handleSubmit = e =>{
        e.preventDefault();

        let formData = new FormData();
        
        if (mountain.name && mountain.rate && mountain.description
            && mountain.itinerary && mountain.information !== ""){
            formData.append('name', mountain.name)
            formData.append('rate', mountain.rate)
            formData.append('description', mountain.description)
            formData.append('itinerary', mountain.itinerary)
            formData.append('information', mountain.information)
        } else{
            count++
            swal({
                    title: "Edit Destination Failed!",
                    text: "Input field must not be empty",
                    icon:"error",
                    Button: "Ok",
                    timer: 3000
                })
        }
        if(mountain.image) {
            formData.append('image', mountain.image);
        }

        fetch(`https://tarabundokbackend.herokuapp.com/mountains/${mountain._id}`,{
            method: "put",
            headers : {
                'Authorization' : `Bearer ${localStorage['appState']}`
            },
            body: formData
        })
        .then( res => res.json())
        .then( data => {
            // if(isError){
                if(count===0){
                  setIsRedirect(true) 
                }
                           // }
            
        })

    }
    useEffect(()=>{
        setMountain({
            ...props.mountain,
            image: ""
        })
    },[props.mountain]);

    if(isRedirect) {
        swal({
                    title: "Edit Destination Successfully",
                    icon:"success",
                    Button: "Ok",
                    timer: 3000
                })
        return <Redirect to="/destination" />
    }

  return (
    <div className="container">
    	<div className="row">
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
    			<form onSubmit={handleSubmit}>
    				<InputGroup
    					name="name"
    					displayName="Mountain Name:"
    					type="text"
                        value={mountain.name}
                        handleChange={handleChange}
    				/>
    				<InputGroup
    					name="rate"
    					displayName="Rate:"
    					type="number"
                        value={mountain.rate}
                        handleChange={handleChange}
    				/>
    				<label htmlFor="description">Description:</label>
    				<textarea 
    						name="description" 
    						id="description" 
    						cols="30" 
    						rows="5"
    						className="form-control"
                            value={mountain.description}
                            onChange={handleChange}></textarea>

    				<label htmlFor="information">Information:</label>
    				<textarea 
    						name="information" 
    						id="information" 
    						cols="30" 
    						rows="5"
    						className="form-control"
                            value={mountain.information}
                            onChange={handleChange}></textarea>

    				<label htmlFor="itinerary">Itinerary:</label>
    				<textarea 
    						name="itinerary" 
    						id="itinerary" 
    						cols="30" 
    						rows="5"
    						className="form-control"
                            value={mountain.itinerary}
                            onChange={handleChange}></textarea>

    				<InputGroup 
    					name ="image"
    					displayName="Image:"
    					type="file"
                        handleChange={handleChangeFile}


    				/>

    				<button className="btn btn-primary my-3">Edit</button>
    			</form>	
    		</div>

    	</div>
    </div>
  )
}

export default MountainEdit;