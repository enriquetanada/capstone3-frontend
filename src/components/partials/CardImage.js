import React from 'react';
import {Link} from 'react-router-dom';
import MountainAdminControl from './MountainAdminControl';
import Modal from './Modal';


const CardImage = ({mountain,setDeletedMountain,authUser, setAuthUser}) => {

  return (
    <>
	    
	    	<div className="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 mx-auto ">
			    <div className="card content">
			    	<div className="content-overlay"></div>
					<img src={`https://tarabundokbackend.herokuapp.com/${mountain.image}`} className="card-img-top " alt="..." height="150" width="250"/>
					<div className="content-details fadeIn-bottom">
		        		<h4 className="content-title">{mountain.itinerary}</h4>
	       				<h5 className="content-text">Rate: &#8369;{mountain.rate}</h5>
		      		</div>
				</div>
				<div className="card-body">
					<h5 className="card-title text-center">{mountain.name}</h5>
					<p className="card-text text-center">Day tour</p>
			
				</div>
				<div className="card-footer">
				{
					authUser && !authUser.isAdmin?
					<Link to={`/books/${mountain._id}`} className="btn btn-success w-100">Book Now!</Link>
					:

					<MountainAdminControl mountain={mountain} setDeletedMountain={setDeletedMountain} id={mountain._id}/>
					
				}

				</div>
			</div>


    </>
  )
}

export default CardImage;