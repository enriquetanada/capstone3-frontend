import React from 'react';

const Modal = ({mountain}) => {
  return (
    <>
<button type="button " class="btn btn-primary w-100 my-1" data-toggle="modal" data-target="#exampleModal{">
  View Destination
</button>

<div class="modal fade" id="exampleModal{" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{mountain.name}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {mountain.information}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</>
  )
}

export default Modal;