import React from 'react';

const TableProductFooter = (props) => {
  return (
    <tfoot>
	    <tr>
	    	<th colSpan="3" className="text-right">Total:</th>
	    	<td>&#8369;_total_</td>
	    	<td>
	    		<button className="btn btn-success">Checkout</button>
	    	</td>
	    </tr>
	</tfoot>
  )
}

export default TableProductFooter;