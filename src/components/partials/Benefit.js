import React from 'react';

const Benefit = (props) => {
  return (
   <> 
   <h1 className="text-center mt-5" id="benefit-home">BENEFITS OF HIKING</h1>
  		<hr />	
   <div className="row  justify-content-center">
    	
  		<div className="mx-5">
  			<i className="fas fa-child fa-10x circle-icon"></i>
  			<h3 className=" text-center my-4" id="benefit-text">Improves Fitness</h3>
  		</div>
  		<div className="mx-5">
  			<i className="fas fa-plus-square fa-9x circle-icon"></i>
        <h3 className="text-center my-4 " id="benefit-text" >Mental Health</h3>

  		</div>
  		<div className="mx-5">
  			<i className="far fa-lightbulb fa-10x circle-icon"></i>
        <h3 className=" text-center my-4" id="benefit-text">Creativity</h3>

  		</div>
  		<div className="mx-5">
  			<i className="fas fa-running fa-10x circle-icon"></i>
        <h3 className=" text-center my-4" id="benefit-text">Improves Stamina</h3>

  		</div>

  	</div>
  	</>
    	
  )
}

export default Benefit;