import React, {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom'

const ReservationHeader = ({reservation, authUser}) => {
    const { id } = useParams();


    const [isComplete, setIsComplete] = useState({
        isComplete: false
    })
    useEffect(()=>{
        fetch(`https://tarabundokbackend.herokuapp.com/reservations/${id}`,{
            headers: {
                "Authorization": `Bearer ${localStorage["appState"]}`
            }
        })
        .then(response => response.json())
        .then( data => {
            setIsComplete(data)
        });
    },[]);

    const handleChange = e =>{
        if(e.target.value==="true"){
            setIsComplete({
                ...isComplete,
                isComplete: true
            })
        }else{
             setIsComplete({
                ...isComplete,
                isComplete: false
            })
        }
        
     }
 

    const handleSubmit = e =>{
        e.preventDefault()

        fetch(`https://tarabundokbackend.herokuapp.com/reservations/${id}`,{
            method: "put",
            body: JSON.stringify(isComplete),
            headers : {
                "Content-Type" : "application/json",
                'Authorization' : `Bearer ${localStorage['appState']}`
            },
        })
        .then(res =>res.json())
        .then(data=>{
            if(data._id){
            console.log(data) 
            }
        })
    }


  return (
    <table className="table">
        <tbody>
        {/*reservation code*/}
        <tr>
            <td>Transcation Code</td>
            <td>{reservation._id}</td>
        </tr>
        {/*customer*/}
        <tr>
            <td>Customer Name</td>
            <td>{ reservation.customerId.fullname}</td>
        </tr>
        {/*purchased date*/}
        <tr>
            <td>Date Reserved</td>
            <td>{reservation.dateReserved}</td>
        </tr>
        {/*status*/}
        <tr>
            <td>Status</td>
            {
                authUser && authUser.isAdmin ?
                <td>
                    <select id="lang" onChange={handleChange}>
                        <option value={false}>Pending</option>
                        <option value={true}>Complete</option>
                    </select> 
                    <button onClick={handleSubmit} className="btn btn-primary"> Edit </button>
                </td>
                :
                <td>{reservation.isComplete ? "Complete" : "Pending"} </td>
            }
        </tr>                      
        </tbody>
    </table>
  )
}

export default ReservationHeader;