import React from 'react';

const Parallax = (props) => {
  return (
    <>
    	<div className="row justify-content-center">
    		<div className="parallax-content ">
    			<i className="fas fa-quote-left fa-2x"></i>
    			<h3 className="parallax-text">It does not matter how slowly you go as long as you DO NOT STOP</h3>
    			<h3 className="text-right mr-3 parallax-text-right">Confucius</h3>
    			<i className="fas fa-quote-right fa-2x fas-right"></i>
    		</div>
    		
    	</div>

    </>
  )
}

export default Parallax;