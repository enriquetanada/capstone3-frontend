import React from 'react';

const TableProductRow = ({order,withAction}) => {
  return (
    <tr>
	    {/*name*/}
	    <td>{order.mountainId.name}</td>
	    {/*price*/}
	    <td>&#8369;{order.rate}</td>
	    {/*quantity*/}
	    <td>{order.person}</td>
	    {/*subtotal*/}
	    <td>{order.subtotal}</td>
	    {
	    	withAction ?
	    		<td><button className="btn btn-outline-danger">Remove</button></td> :
	    		<></>
	    }
	    
	</tr>	
  )
}

export default TableProductRow;