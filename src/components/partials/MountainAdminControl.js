import React from 'react';
import {Link} from 'react-router-dom';
import swal from 'sweetalert';

const MountainAdminControl = ({id,mountain, setDeletedMountain}) => {

	const handleClick =() =>{
		fetch(`https://tarabundokbackend.herokuapp.com/mountains/${id}`,{
			method: "delete",
			headers:{
				'Authorization' : `Bearer ${localStorage['appState']}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			if(setDeletedMountain){
				swal({
                    title: "Deleted Successfully",
                    icon:"success",
                    Button: "Ok",
                    timer: 3000
                })
            	setDeletedMountain({_id: id})
            }
		})
	}
  return (
    <>
    	<Link to={`/mountains/${mountain._id}/edit`} className="btn btn-warning my-1 w-100">Edit</Link>
    	<button className="btn btn-danger my- w-100" onClick={handleClick}>Delete</button>
    </>
  )
}

export default MountainAdminControl;