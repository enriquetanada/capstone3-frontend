import React, {useState, useEffect} from 'react';
import CardImage from './partials/CardImage'

const Destination = ({authUser, setAuthUser}) => {

	const [mountains, setMountain] = useState([])
  const [deletedMountain, setDeletedMountain] = useState({})


	useEffect(()=>{
		fetch("https://tarabundokbackend.herokuapp.com/mountains")
		.then(res => {return res.json()})
		.then(mountains=>setMountain(mountains))
	},[])

  useEffect(()=>{
    if(deletedMountain){
      setMountain(mountains.filter( mountain => {
        return mountain._id !== deletedMountain._id
      }))
    }
  },[deletedMountain]); 

	let mountainList = mountains.map(mountain=>(
		<CardImage 
      mountain={mountain} 
      setDeletedMountain={setDeletedMountain} 
      authUser={authUser} 
      setAuthUser={setAuthUser}
    />
	))
  return (
    <div className="container">
    <h1 className="text-center" id="body-home">DESTINATIONS</h1>
  	<hr />
  	<h4 className="text-center">Explore our latest destinations</h4>
  	<div className="row mt-5">
    	{mountainList}
    </div>
    </div>
  )
}

export default Destination;