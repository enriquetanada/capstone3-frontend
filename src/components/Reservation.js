import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';

const Reservation = (props) => {

    const [reservations, setReservations] = useState([]);

    useEffect( () => {
      fetch("https://tarabundokbackend.herokuapp.com/reservations",{
            headers:{
                "Authorization" : `Bearer ${localStorage['appState']}`
            }
        })
        .then(response => response.json())
        .then(data =>{
            setReservations(data)
        })  
    },[])

    let transactionList = reservations.map(transaction =>{
        return (
            <li className="list-group-item" key={transaction._id}>
                <Link to={`/reservations/${transaction._id}`}>
                    {transaction._id}
                    {
                        transaction.isComplete ? 
                      <span 
                        className="badge badge-success ml-3"
                        >
                    {
                        transaction.isComplete ? "Complete" : "Pending"
                    }
                    </span> 
                    :
                      <span 
                        className="badge badge-danger ml-3"
                        >
                    {
                        transaction.isComplete ? "Complete" : "Pending"
                    }
                    </span> 

                    }
                    
                </Link>
            </li>            
        )
    })
  return (
    <div className="container">
    	<div className="row">
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
    			 <ul className="list-group">
                      {transactionList}  
                    </ul>
    		</div>
    	</div>
    </div>
  )
}

export default Reservation;